#
# CookbookName:: ihg-service
# Recipe:: start_gemfire
#
# Copyright:: 2018, IHG, All Rights Reserved.
#

cb_version = run_context.cookbook_collection[cookbook_name].version
log "#{node['log']['separator']}IHG Begin Recipe: #{cookbook_name}::#{recipe_name}@#{cb_version} on Node: #{node.name} Environment: #{node.chef_environment}#{node['log']['separator']}"

case node['Datacenter']
when 'ADC'
  ltag = node['Workgroup'] + 'lc'
  ctag = node['Workgroup'] + 'dg'
else
  ltag = 'lc'
  ctag = 'dg'
end
puts node['tags']

if node['tags'].include? ltag
  service 'gemfireloc' do
    supports :status => false
    action :start
  end

  case node['Datacenter']
  when 'ADC'
    bash 'describe members for ADC locator' do
      code <<-EOH
        function get_time() {
          date +%s
        }
        function flush_txts() {
          rm /tmp/lc_member.txt 2>&-
          rm /tmp/adc_member.txt 2>&-
          rm /tmp/raw_adc_lc1.txt 2>&-
          rm /tmp/raw_adc_lc2.txt 2>&-
          rm /tmp/raw_adc_host.txt 2>&-
          rm /tmp/raw_adc_member1.txt 2>&-
          rm /tmp/raw_adc_member2.txt 2>&-
          rm /tmp/member.txt 2>&-
          rm /tmp/raw_lc1.txt 2>&-
          rm /tmp/raw_host.txt 2>&-
          rm /tmp/raw_member1.txt 2>&-
          rm /tmp/raw_lc2.txt 2>&-
          rm /tmp/raw_member2.txt 2>&-
          rm /tmp/raw_jp.txt 2>&-
        }
        function handle_loc() {
          echo "locator will be handled here"
          while true; do
            echo "$1" > /tmp/lc_member.txt
            NOW=$(get_time)
            if [ $NOW -ge $STOP ];then
              echo -e "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nERROR:  Gemifire Locator is taking longer than expected time to come up.\nExiting..."
              cd $(find /apps/home/gemfire/ -wholename "*scripts/stop_locator.sh" | sed 's/scripts.*$//')
              dzdo -u gemfire $(find /apps/home/gemfire/ -wholename "*scripts/stop_locator.sh")
              exit 1
            else
              echo "Waiting for up to an hour for the GermFire Locator to come up..."
              CO=`export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect " -e "describe member --name=$1" &`
              echo 'command output:'$CO
              echo "MB="`cat /tmp/lc_member.txt`
              if [ -z "$CO" ]; then
                echo "There was no output for describe member"
              else
                echo "else condition success"
                if `echo "$CO" |egrep 'Locators|PID' > /dev/null`; then
                  if `netstat -aon|grep '10334.*LISTEN' > /dev/null`; then
                    echo "Good Condition"
                    echo -e "\n###########################\nThe Gemifire Locator is really up.\n###########################\n"
                    exit
                  else
                    echo "Port 10334 is not listening, hence repeating the check"
                    continue
                  fi
                else
                  echo "Locator & PID Not found from Describe member, hence repeating the check"
                  sleep 30
                fi
              fi
            fi
          done
        }
        flush_txts
        START=$(get_time)
        DURATION=1800   # in seconds
        STOP=$(expr $START + $DURATION)
        ps -ef |grep -v grep |egrep 'geode|gemfire.jar' |grep "java"| egrep 'locator-dependencies.jar|geode-dependencies.jar'| awk '{print $8}' > /tmp/raw_jp.txt
        JP=`uniq /tmp/raw_jp.txt`
        if `hostname|egrep ".ihgint.global|.ihgext.global|.hiw.com" > /dev/null`;then
          HN=$(hostname|sed "s/[.].*//")
        else
          HN=$(hostname)
        fi
        MB=$(export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect " -e 'list members' | awk '{print $1}' | grep "$HN")
        if [ `echo "$MB"|wc -l` == 2 ];then
          fMB=$(echo "$MB"|head -1)
          if [ `echo "$fMB"|grep "loc$"` ];then
            handle_loc $fMB
          fi
          sMB=$(echo "$MB"|tail -1)
          if [ `echo "$sMB"|grep "loc$"` ];then
            handle_loc $sMB
          fi
        else
          handle_loc $MB
        fi
      EOH
      timeout 1800
    end
  else
    bash 'describe members for IADD1 & SC9 locator' do
      code <<-EOH
        function get_time() {
          date +%s
        }
        function flush_txts() {
          rm /tmp/lc_member.txt 2>&-
          rm /tmp/member.txt 2>&-
          rm /tmp/raw_lc1.txt 2>&-
          rm /tmp/raw_host.txt 2>&-
          rm /tmp/raw_member1.txt 2>&-
          rm /tmp/raw_lc2.txt 2>&-
          rm /tmp/raw_member2.txt 2>&-
          rm /tmp/raw_jp.txt 2>&-
        }
        function handle_loc() {
          echo "locator will be handled here"
          while true; do
            echo "$1" > /tmp/lc_member.txt
            NOW=$(get_time)
            if [ $NOW -ge $STOP ];then
              echo -e "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nERROR:  Gemifire Locator is taking longer than expected time to come up.\nExiting..."
              cd $(find /apps/home/gemfire/ -wholename "*scripts/stop_locator.sh" | sed 's/scripts.*$//')
              dzdo -u gemfire $(find /apps/home/gemfire/ -wholename "*scripts/stop_locator.sh")
              exit 1
            else
              echo "Waiting for up to an hour for the GermFire Locator to come up..."
              CO=`export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect " -e "describe member --name=$1" &`
              echo 'command output:'$CO
              echo "MB="`cat /tmp/lc_member.txt`
              if [ -z "$CO" ]; then
                echo "There was no output for describe member"
              else
                echo "else condition success"
                if `echo "$CO" |egrep 'Locators|PID' > /dev/null`; then
                  if `netstat -aon|grep '10334.*LISTEN' > /dev/null`; then
                    echo "Good Condition"
                    echo -e "\n###########################\nThe Gemifire Locator is really up.\n###########################\n"
                    exit
                  else
                    echo "Port 10334 is not listening, hence repeating the check"
                    continue
                  fi
                else
                  echo "Locator & PID Not found from Describe member, hence repeating the check"
                  sleep 30
                fi
              fi
            fi
          done
        }
        flush_txts
        START=$(get_time)
        DURATION=1800   # in seconds
        STOP=$(expr $START + $DURATION)
        ps -ef |grep -v grep |egrep 'geode|gemfire.jar' |grep "java"| egrep 'locator-dependencies.jar|geode-dependencies.jar'| awk '{print $8}' > /tmp/raw_jp.txt
        JP=`uniq /tmp/raw_jp.txt`
        if `hostname|egrep ".ihgint.global|.ihgext.global|.hiw.com" > /dev/null`;then
          HN=$(hostname|sed "s/[.].*//")
        else
          HN=$(hostname)
        fi
        MB=$(export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect " -e 'list members' | awk '{print $1}' | grep "$HN")
        if [ `echo "$MB"|wc -l` == 2 ];then
          fMB=$(echo "$MB"|head -1)
          if [ `echo "$fMB"|grep "loc$"` ];then
            handle_loc $fMB
          fi
          sMB=$(echo "$MB"|tail -1)
          if [ `echo "$sMB"|grep "loc$"` ];then
            handle_loc $sMB
          fi
        else
          handle_loc $MB
        fi
      EOH
      timeout 1800
    end
  end
end

if node['tags'].include? ctag
  puts "\nCache Server is true:  #{ctag}\n"
  service 'gemfirecs' do
    supports :status => false
    action :start
  end

  case node['Datacenter']
  when 'ADC'
    bash 'describe members for ADC cache' do
      code <<-EOH
        function get_time() {
          date +%s
        }
        function flush_txts() {
          rm /tmp/lc_member.txt 2>&-
          rm /tmp/lc_member.txt 2>&-
          rm /tmp/adc_member.txt 2>&-
          rm /tmp/raw_adc_lc1.txt 2>&-
          rm /tmp/raw_adc_lc2.txt 2>&-
          rm /tmp/raw_adc_host.txt 2>&-
          rm /tmp/raw_adc_member1.txt 2>&-
          rm /tmp/raw_adc_member2.txt 2>&-
          rm /tmp/member.txt 2>&-
          rm /tmp/raw_lc1.txt 2>&-
          rm /tmp/raw_host.txt 2>&-
          rm /tmp/raw_member1.txt 2>&-
          rm /tmp/raw_lc2.txt 2>&-
          rm /tmp/raw_host.txt 2>&-
          rm /tmp/raw_member2.txt 2>&-
          rm /tmp/raw_jp.txt 2>&-
        }
        function handle_dg() {
          echo "cache will be handled here"
          while true; do
            echo "$1 and $2" > /tmp/member.txt
            CO=`export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=$2" -e "describe member --name=$1" &`
            echo 'command output:'$CO
            echo "MB="`cat /tmp/member.txt`
            NOW=$(get_time)
            if [ $NOW -ge $STOP ];then
              echo -e "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nERROR:  Gemifire Server is taking longer than expected time to come up.\nExiting..."
              cd $(find /apps/home/gemfire/ -wholename "*scripts/stop_server.sh" | sed 's/scripts.*$//')
              dzdo -u gemfire $(find /apps/home/gemfire/ -wholename "*scripts/stop_server.sh")
              exit 1
            else
              echo "Waiting for up to an hour for the GermFire Server to come up..."
              CC=`export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=$2" -e "describe member --name=$1" | grep "^Client Connections" | awk '{print $4}'`
              echo CC=$CC
              #echo CO=$CO
              if [ -z "$CO" ];then
                echo "There was no output for describe member"
              else
                echo "else condition success"
                echo "CO value is ":$CO
                if `echo "$CO" | tr '\n' ' ' | sed 's/  */ /g' | grep 'Regions : [a-zA-Z0-9_\.][a-zA-Z0-9_\.]* .*PID : [0-9][0-9]*' > /dev/null`; then
                  if `netstat -aon|grep '40404.*LISTEN' > /dev/null`;then
                    echo "Good Condition"
                    echo -e "\n###########################\nThe Gemifire Server is really up.\n###########################\n"
                    exit
                  else
                    echo "Not listening on port 40404, hence repeating the check"
                    sleep 5
                    continue
                  fi
                else
                  echo "Region & PID Not found from Describe member, hence repeating the check"
                  sleep 30
                fi
              fi
            fi
          done
        }
        flush_txts
        START=$(get_time)
        DURATION=3600   # in seconds
        STOP=$(expr $START + $DURATION)
        ps -ef |grep -v grep |egrep 'geode|gemfire.jar' |grep "java"| egrep 'server-dependencies.jar|geode-dependencies.jar'| awk '{print $8}' > /tmp/raw_jp.txt
        JP=`uniq /tmp/raw_jp.txt`
        echo JP=$JP
        if [ ! -z "$JP" ];then
          if `hostname|egrep ".ihgint.global|.ihgext.global|.hiw.com" > /dev/null`;then
            HN=$(hostname|sed "s/[.].*//")
          else
            HN=$(hostname)
          fi
          LC1=$(find /apps/home/gemfire/ -wholename "*config/cache.properties" -exec grep locator {} \\; | awk -F'=' '{print $2}'|awk -F',' '{print $1}'| uniq)
          LC2=$(find /apps/home/gemfire/ -wholename "*config/cache.properties" -exec grep locator {} \\; | awk -F'=' '{print $2}'|awk -F',' '{print $2}'| uniq)
          echo "LC1="$LC1
          echo "LC2="$LC2
          MB=$(export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC1}" -e 'list members' | awk '{print $1}' | grep "$HN" 2&> /dev/null)
          echo "${LC1}" > /tmp/raw_lc1.txt
          echo "${HN}" > /tmp/raw_host.txt
          export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC1}" -e 'list members' | awk '{print $1}' | grep "$HN" > /tmp/raw_member1.txt
          MB=`cat /tmp/raw_member1.txt`
          if [ ! -z "$MB" ];then
            if [ `echo "$MB"|wc -l` == 2 ];then
              fMB=$(echo "$MB"|head -1)
              echo lc1_fMB=$fMB
              if [ `echo "$fMB" | grep "cs$"` ];then
                handle_dg $fMB $LC1
              fi
              sMB=$(echo "$MB"|tail -1)
              echo lc1_sMB=$sMB
              if [ `echo "$sMB" | grep "cs$"` ];then
                handle_dg $sMB $LC1
              fi
            else
              echo lc1_MB=$MB
              echo lc1_MB_wc=`echo "$MB"|wc -l`
              handle_dg $MB $LC1
            fi
          else
            MB=$(export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC2}" -e 'list members' | awk '{print $1}' | grep "$HN" 2&> /dev/null)
            echo "${LC2}" > /tmp/raw_lc2.txt
            echo "${HN}" > /tmp/raw_host.txt
            export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC2}" -e 'list members' | awk '{print $1}' | grep "$HN" > /tmp/raw_member2.txt
            MB=`cat /tmp/raw_member2.txt`
            if [ `echo "$MB"|wc -l` == 2 ];then
              fMB=$(echo "$MB"|head -1)
              echo lc2_fMB=$fMB
              if [ `echo "$fMB" | grep "cs$"` ];then
                handle_dg $fMB $LC2
              fi
              sMB=$(echo "$MB"|tail -1)
              echo lc2_sMB=$sMB
              if [ `echo "$sMB" | grep "cs$"` ];then
                handle_dg $sMB $LC2
              fi
            else
              echo lc2_MB=$MB
              echo lc2_MB_wc=`echo "$MB"|wc -l`
              handle_dg $MB $LC2
            fi
          fi
        else
          echo -e "\n#########################\nThe Gemifire Server cannot be brought up.\n##########################\n"
          exit 1
        fi
      EOH
      timeout 3600
    end
  else
    bash 'describe members for IADD1 or SJCD1 cache' do
      code <<-EOH
        function get_time() {
          date +%s
        }
        function flush_txts() {
          rm /tmp/lc_member.txt 2>&-
          rm /tmp/adc_member.txt 2>&-
          rm /tmp/member.txt 2>&-
          rm /tmp/raw_lc1.txt 2>&-
          rm /tmp/raw_host.txt 2>&-
          rm /tmp/raw_member1.txt 2>&-
          rm /tmp/raw_lc2.txt 2>&-
          rm /tmp/raw_member2.txt 2>&-
          rm /tmp/raw_jp.txt 2>&-
        }
        function handle_dg() {
          echo "cache will be handled here"
          while true; do
            echo "$1 and $2" > /tmp/member.txt
            CO=`export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=$2" -e "describe member --name=$1" &`
            echo 'command output:'$CO
            echo "MB="`cat /tmp/member.txt`
            NOW=$(get_time)
            if [ $NOW -ge $STOP ];then
              echo -e "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nERROR:  Gemifire Server is taking longer than expected time to come up.\nExiting..."
              cd $(find /apps/home/gemfire/ -wholename "*scripts/stop_server.sh" | sed 's/scripts.*$//')
              dzdo -u gemfire $(find /apps/home/gemfire/ -wholename "*scripts/stop_server.sh")
              exit 1
            else
              echo "Waiting for up to an hour for the GermFire Server to come up..."
              CC=`export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=$2" -e "describe member --name=$1" | grep "^Client Connections" | awk '{print $4}'`
              echo CC=$CC
              #echo CO=$CO
              if [ -z "$CO" ];then
                echo "There was no output for describe member"
              else
                echo "else condition success"
                echo "CO value is ":$CO
                if `echo "$CO" | tr '\n' ' ' | sed 's/  */ /g' | grep 'Regions : [a-zA-Z0-9_\.][a-zA-Z0-9_\.]* .*PID : [0-9][0-9]*' > /dev/null`; then
                  if `netstat -aon|grep '40404.*LISTEN' > /dev/null`;then
                    echo "Good Condition"
                    echo -e "\n###########################\nThe Gemifire Server is really up.\n###########################\n"
                    exit
                  else
                    echo "Not listening on port 40404, hence repeating the check"
                    sleep 5
                    continue
                  fi
                else
                  echo "Region & PID Not found from Describe member, hence repeating the check"
                  sleep 30
                fi
              fi
            fi
          done
        }
        flush_txts
        START=$(get_time)
        DURATION=3600   # in seconds
        STOP=$(expr $START + $DURATION)
        ps -ef |grep -v grep |egrep 'geode|gemfire.jar' |grep "java"| egrep 'server-dependencies.jar|geode-dependencies.jar'| awk '{print $8}' > /tmp/raw_jp.txt
        JP=`uniq /tmp/raw_jp.txt`
        echo JP=$JP
        if [ ! -z "$JP" ];then
          if `hostname|egrep ".ihgint.global|.ihgext.global|.hiw.com" > /dev/null`;then
            HN=$(hostname|sed "s/[.].*//")
          else
            HN=$(hostname)
          fi
          LC1=$(find /apps/home/gemfire/ -wholename "*config/cache.properties" -exec grep locator {} \\; | awk -F'=' '{print $2}'|awk -F',' '{print $1}'| uniq)
          LC2=$(find /apps/home/gemfire/ -wholename "*config/cache.properties" -exec grep locator {} \\; | awk -F'=' '{print $2}'|awk -F',' '{print $2}'| uniq)
          echo "LC1="$LC1
          echo "LC2="$LC2
          MB=$(export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC1}" -e 'list members' | awk '{print $1}' | grep "$HN" 2&> /dev/null)
          echo "${LC1}" > /tmp/raw_lc1.txt
          echo "${HN}" > /tmp/raw_host.txt
          export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC1}" -e 'list members' | awk '{print $1}' | grep "$HN" > /tmp/raw_member1.txt
          MB=`cat /tmp/raw_member1.txt`
          if [ ! -z "$MB" ];then
            if [ `echo "$MB"|wc -l` == 2 ];then
              fMB=$(echo "$MB"|head -1)
              echo lc1_fMB=$fMB
              if [ `echo "$fMB" | grep "cs$"` ];then
                handle_dg $fMB $LC1
              fi
              sMB=$(echo "$MB"|tail -1)
              echo lc1_sMB=$sMB
              if [ `echo "$sMB" | grep "cs$"` ];then
                handle_dg $sMB $LC1
              fi
            else
              echo lc1_MB=$MB
              echo lc1_MB_wc=`echo "$MB"|wc -l`
              handle_dg $MB $LC1
            fi
          else
            MB=$(export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC2}" -e 'list members' | awk '{print $1}' | grep "$HN" 2&> /dev/null)
            echo "${LC2}" > /tmp/raw_lc2.txt
            echo "${HN}" > /tmp/raw_host.txt
            export JAVA_HOME=${JP%/bin/java}; gfsh -e "connect --locator=${LC2}" -e 'list members' | awk '{print $1}' | grep "$HN" > /tmp/raw_member2.txt
            MB=`cat /tmp/raw_member2.txt`
            if [ `echo "$MB"|wc -l` == 2 ];then
              fMB=$(echo "$MB"|head -1)
              echo lc2_fMB=$fMB
              if [ `echo "$fMB" | grep "cs$"` ];then
                handle_dg $fMB $LC2
              fi
              sMB=$(echo "$MB"|tail -1)
              echo lc2_sMB=$sMB
              if [ `echo "$sMB" | grep "cs$"` ];then
                handle_dg $sMB $LC2
              fi
            else
              echo lc2_MB=$MB
              echo lc2_MB_wc=`echo "$MB"|wc -l`
              handle_dg $MB $LC2
            fi
          fi
        else
          echo -e "\n#########################\nThe Gemifire Server cannot be brought up.\n##########################\n"
          exit 1
        fi
      EOH
      timeout 3600
    end
  end
end
