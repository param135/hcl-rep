ihg-service CHANGELOG
=====================

This file is used to list changes made in each version of the ihg-service cookbook.

1.4.59
-[Parameshwara] May 27, 2021
  Handled multiple locators issue by unique value

1.4.58
-[Parameshwara] May 27, 2021
  Expanded java criteria for locators as well

1.4.57
-[Parameshwara] May 27, 2021
  Expanded java criteria for starting gemfire

1.4.56
-[Daniel Browne] May 25, 2021
  Add default functionality to common_postpatch.rb ADHOC0259459

1.4.55
-[Daniel Browne] May 11, 2021
  Removed ihg-springboot-base from start/stop patch scripts

1.4.54
-[Revanth Gudimalla] May 11, 2021
  Removed eda_replicator from EDA patching process

1.4.53
-[Daniel Browne]
  update ihg-springboot-base version

1.4.52
-[Daniel Browne]
  update ihg-springboot-base version

1.4.51
-[Daniel Browne]
  set ihg-springboot-base version requirement

1.4.50
-[Daniel Browne]
  Added Berksfile

1.4.49
-[Daniel Browne]
  Adding support for springboot to patching start & stop app

1.4.48
-[Parameshwara] Mar 08, 2021
  Last offense related to trailing blank lines detected handled

1.4.47
-[Parameshwara] Mar 08, 2021
  Additional offenses handled

1.4.46
-[Parameshwara] Mar 08, 2021
  Handled the offenses generated by the linting tool

1.4.45
-[Parameshwara] Mar 08, 2021
  Corrections and enhancement around Gemfire startup method

1.4.44
-[Daniel Browne] Nov 11, 2020
  Removed broken tcserver kill block from stop_tcserver

1.4.43
-[Revanth] Oct 22, 2020
  Version Bump

1.4.42
-[Revanth] Oct 22, 2020
  Removed whitespaces in recipes

1.4.41
-[Revanth] Oct 22, 2020
  Removed whitespacing in recipes

1.4.40
-[Revanth] Oct 22, 2020
  Added Stop/Start Recipes for EDA services and removed depenency cookbook

1.4.39
-[HarishB] Sept 18, 2020
  Updated stop_eda and start_eda recipes with correct attribute match

1.4.38
-[Revanth] Sept 16, 2020
  updated code in stopapp_prepatch

1.4.37
-[Revanth] Sept 15, 2020
  Removed version pinning in metadata for EDA cookbook

1.4.36
-[Revanth] Sept 15, 2020
  Added start/stop recipes for EDA servers and Biztier application

1.4.35
-[Revanth] Sept 9, 2020
  Version Bump

1.4.34
-[Revanth] Sep 3, 2020
  EDA scripts put on hold until dependency cookbook issues are solved

1.4.33
-[Revanth] August 26, 2020
  Updated EDA replicator steps

1.4.32
-[Revanth] August 26, 2020
  modified delivery build cookbook

1.4.31
-[Revanth] August 26, 2020
  pinned versions for dependencies in metadata

1.4.30
-[Revanth] August 26, 2020
  Version bump

1.4.29
-[Revanth] August 26, 2020
  Added Start and Stop Recipes for EDA App Servers
  
1.4.28
-[Parameshwara] July 10, 2020
  Releasing Gemfire for non-ADC to Prod

1.4.27
-[Revanth] July 9, 2020
  Remove bluestripe from Patch process

1.4.26
-[Parameshwara] July 9, 2020
  further updated the start_gemfire.rb to handle more validation and limited to production

1.4.25
-[Revanth] July 7, 2020
  Added if statement in stopapp_prepatch recipe to check if bluestripe is installed before running destroy cookbook

1.4.24
-[Parameshwara] June 30, 2020
  update start_gemfire.rb to use new validations when the services of gemfire comes up

1.4.23
-[Daniel Browne] June 14, 2020
  update get_connections.rb to use new bigip_watchnodeconnections call which handles all the logic for draining connections

1.4.22
-[Daniel Browne] June 9, 2020
  fix typo in get_connection.rb

1.4.21
-[Daniel Browne] June 9, 2020
  update get_connections.rb to use bigip_checkconnections instead of bigip_getnodeconnections so the local cookbook logic works correctly

1.4.20
-[Daniel Browne] June 7, 2020
  update get_connections.rb to fix 5 min timeout not working. Also updated cutoff to 20% instead of 10%.
  updated .delivery to match other cookbooks to resolve failures in chef delivery build step.

1.4.19
-[Daniel Browne] May 31, 2020
  update bigpanda uptime/downtime URLs in stopapp_prepatch & check_healthmonitors_uptime

1.4.18
-[Daniel Browne] May 13, 2020
  update get_connections.rb to timeout after 5 minutes if connections do not reach 0.

1.4.17
-[Daniel Browne] March 4, 2020
  update f5-icontrol-install.rb to request a version of chef-vault gem compatile with our environment

1.4.16
-[Daniel Browne] April 19, 2020
    update F5 scripts to no longer use svrbuild user and instead store files in /tmp/

1.4.1r
-[Birendra Singh] June 18, 2019
    Typop fixed.

1.4.15
-[Birendra Singh] June 18, 2019
    Update check_repo_connectivity.rb to account for new file system based yum repos 

1.4.13
-[Don Harden] Feb 28, 2109
    Increased time for Ambari to start back up.  Changed Chef vault lookup for Ambari VMs.

1.4.12
-[Don Harden] Feb 21, 2109
    Remove unneeded Ambari and Hadoop repos.  Increased time for Ambari to start back up.

1.4.11
-[Don Harden] Feb 11, 2109
  Fixed linting -

1.4.10
-[Don Harden] Feb 11, 2109
	US61514: BDC Patching :: Rundeck Automation - Phase 1 Added start_ambari, and stop_ambari recipies and helper library

1.4.9
-[Don Harden] Sep 20, 2108
US49558: If in ADC use the ADC Artifactory as source for gems

1.4.8
-[Venkata Anusha Mahankali]
Point Public cloud servers to iadd1 master repo server

1.4.7
-[Don Harden] Sep 4, 2108
Add "ignore_failure true" to stop_gemfire and stop_jboss recipes.

1.4.6
-[Don Harden]
Removed Gemfire locator and cache service gfsh availibilty checks in start_gemfire.rb

1.4.5
-[Don Harden]
Fix metadata.rb

1.4.4
-[Don Harden]
Added recipes start_gemfire.rb, stop_gemfire.rb, start_jboss.rb, stop_jboss.rb.

1.4.3
-[Don Harden]
Added checks and expanded rescues to account for exceptions. Also placed BigPanda uptime in a def.

1.4.2
-[Don Harden]
Modified check_healthmonitors_uptime and stopapp_prepatch recipies to get VM health status form AppDynamics and downtime or uptime VM alerts in BigPanda.
Also fixed various Rubocop and Foodcritic violations.

1.4.1
-[Venkata Anusha Mahankali]
Included  recipes to automate start/stop for Psoft servers

1.4.0
-[Venkata Anusha Mahankali]
 Included Recipes to create pool and vip in public cloud

1.3.141
-[Kerrington Wells]
Chaning timeout to 15 minutes and ipsoft check to 60 seconds instead of 15 seconds.

1.3.140
-[Kerrington Wells]
Created default template if one doesn't exist on adcrep01.


1.3.139
-[Venkata Anusha Mahankali]
Include api call bigip_get_vipinfo

1.3.138
-[Venkata Anusha Mahankali]
include api call bigip_checkconnections

1.3.137
-[Venkata Anusha Mahankali]
included check_connections recipe to provide F5 connection count and exit

1.3.136
-[Kerrington Wells]
Only removing ldap-client instead of ldap server.

1.3.135
-[Kerrington Wells]
Made ldap packages string literals instead of varaiable names.

1.3.134
-[Kerrington Wells]
Fixed package removal code for ldap.

1.3.133
-[Kerrington Wells]
Added Jenkins to removal list for passwd and shadow files. Also included removal of ladap client and servers for all data
centers except atl.


1.3.132
-[Kerrington Wells]
Updated file permissions logic in CIS_remediation recepie.

1.3.131
-[Kerrington Wells]
Added remediation for permissions on home directories.


1.3.130
-[Kerrington Wells]
Removed repeadted lines.


1.3.129
-[Kerrington Wells]
Fixed issue with /etc/inspec directory failing to generate


1.3.128
-[Kerrington Wells]
Removing mrich and builduser from both /etc/passwd and /etc/shadow files.

1.3.127
-[Kerrington Wells]
Updating version.

1.3.126
-[Kerrington Wells]
Updated Cis remediation script to include rsyslog permissions setting.


1.3.125
-[Venkata Anusha Mahankali]
Modified get_connections recipe

1.3.124
-[Kerrington Wells]
Added updates to CIS remediation recipie that updates /dev/shm

1.3.123
-[Kerrington Wells]
Added CIS remediation recipie.

1.3.122
-[Venkata Anusha Mahankali]
Removed test recipes for F5

1.3.121
-[venkata Anusha Mahankali]
modify get node connections api call to provide active f5 ip instead of standby

1.3.120
-[Kerrington Wells]
Incrementing version to resubmit through delivery

1.3.119
-[Venkata Anusha Mahankali]
Included a recipe to test conncetivity to production f5

1.3.118
-[Venkata Anusha Mahankali]
modified check repo connectivity recipe to get the releasedate from node['releasedate']

1.3.117
-[Venkata Anusha Mahankali]
modified stopapp_prepatch to get the datacenter attribute from node['Datacenter'] instead of node['datacenter']['name']

1.3.116
-[Venkata Anusha Mahankali]
 Get F5 ips , disable node , get connections

1.3.113
Find the standby f5 and enable and disable node

1.3.112
-[Venkata anusha Mahankali]
test get fail over state api

1.3.111
-[Venkata Anusha Mahankali]
  find f5 ips of a node

1.3.110
-[Venkata Anusha Mahankali]
test bigip_getnodeconnections

1.3.109
-[Venkata Anusha Mahankali]
test updated bigip library

1.3.108
-[Venkata Anusha Mahankali]
added recipe to get the node connections for f5

1.3.107
-[Venkata Anusha Mahankali]
added all the functionality to check repo folders for centos and redhat

1.3.106
-[Venkata Anusha Mahankali]
update check repo recipe

1.3.105
-[Venkata Anusha Mahankali]
get the reposervers from the databag

1.3.104
-[Venkata Anusha Mahankali]
updated check_repo_connectivity recipe

1.3.103
-[Kerrington Wells]
Changed placement of netstat. Moved netstat to checkmonitors recipie.

1.3.102
-[Kerrington Wells]
Removed sudo from netstat command in startapp and stopapp.

1.3.101
-[Venkata Anusha Mahankali]
Test api calls of ihg-f5

1.3.100
-[Kerrington Wells]
Added logging for processes before and after

1.3.97
-[Venkata Anusha Mahankali]
Include a recipe to check repo server conncetivity

1.3.96
-[Venkata Anusha Mahankali]
 make bigip_findpools api call without node info
1.3.95
----------
-[Venkata Anusha Mahankali]
  Offline a node in staging

1.3.94
----------
-[Venkata Anusha Mahankali]
  Pass ip address to find pools

1.3.93
----------
-[Kerrington Wells]
  Added chef-vault gem.

1.3.92
----------
-[Kerrington Wells]
  Added ambari stop and start scripts.

1.3.91
----------
-[Kerrington Wells]
  Added ambari stop and start scripts.


1.3.90
----------
-[Venkata Anusha Mahankali]
  Add httpclient and soap4r-spox gems

1.3.89
----------
-[Venkata Anusha Mahankali]
  Add recipe to install f5-icontrol gem

1.3.88
-------
-[Venkata Anusha Mahankali]
  Test disable/enable nodes in both Public and stag env

1.3.87
-------
-[Venkata Anusha Mahankali]
  Get the list of pools the node is a member of

1.3.86
-----
-[Kerrington Wells]
  Added node.save to stoppapp_prepatch

1.3.85
-----
-[Kerrington Wells]
  Added edge-router to start.


1.3.84
-----
-[Kerrington Wells]
  Added apigee edge router to services array.


1.3.83
-----
-[Kerrington Wells]
  Added new apigee services.


1.3.82
-----
-[Kerrington Wells]
  Fixed search logic for apigee services.


1.3.81
-----
-[Kerrington Wells]
  Changed apagiee zookeeper name to correct name.

1.3.80
-----
-[Kerrington Wells]
  Added message processor to framework.

1.3.79
-----
-[Kerrington Wells]
  Created addition apigee scripts that start and stop each induvidual apigee service.

1.3.78
-----
-[Kerrington Wells]
  Created addition apigee scripts that start and stop each induvidual apigee service.

1.3.75
-----
-[Kerrington Wells]
  Added apigee services to apigee start all.

1.3.74
-----
-[Kerrington Wells]
  Now listing services as they are added to the services attribute.

1.3.73
-----
-[Kerrington Wells]
  Added chkconfig as determing factor for services catalog.

1.3.72
-----
-[Kerrington Wells]
  Added colorized themes to output.

1.3.71
-----
-[Kerrington Wells]
  Added additional checks for apigee nodes.


1.3.70
-----
-[Kerrington Wells]
  Added stderr to output variable.

1.3.69
-----
-[Kerrington Wells]
  Removed Counter in apigee stop.

1.3.68
-----
-[Kerrington Wells]
  Cleaned up Apigee output.

1.3.67
-----
-[Kerrington Wells]
  Added is "not running" to else if statment.

1.3.66
-----
-[Kerrington Wells]
  Added stop and start for apigee zookeer and Cassandra services.


1.3.65
-----
-[Venkata Anusha Mahankali]
  Run Verify services Before start/Stop.

1.3.64
-----
-[Venkata Anusha Mahankali]
  Enable/Disable Pool members in Staging 1.

1.3.63
-----
-[Venkata Anusha Mahankali]
  Get the list of pools and disable the node in each pool.

1.3.62
-----
-[Venkata Anusha Mahankali]
  Pass port number to bigip_pool method.

1.3.61
-----
-[Venkata Anusha Mahankali]
  Pass port number to bigip_pool method.

1.3.60
-----
-[Venkata Anusha Mahankali]
  Added functionality to get the port number to enable/disable.

1.3.59
-----
-[Kerrington Wells]
  Added enable and disable functionality for entire pool.

1.3.58
-----
-[Kerrington Wells]
  Adding ADC and MKSD1 to stopapp_prepatch recipie part 2

1.3.57
-----
-[Venkata Anusha Mahankali]
  Removed f5-iocontrol gem file from the cookbook

1.3.56
-----
-[Kerrington Wells]
  Adding enable node receipe

1.3.55
-----
-[Kerrington Wells]
  Test Offline pool member


1.3.54
-----
-[Venkata Anusha Mahankali]
  Test Offline pool member

1.3.53
-----
-[Venkata Anusha Mahankali]
  Added f5-iocontrol gem

1.3.52
-----
-[Venkata Anusha Mahankali]
  Modified test_vips Recipe to enable a pool member.

1.3.51
-----
-[Venkata Anusha Mahankali]
  Included Recipe to add a pool member.


1.3.50
-----
-[Venkata Anusha Mahankali]
  Included Recipe test_vips.

1.3.49
-----
-[Kerrington Wells]
  Removed setting on relesaedate.


1.3.48
-----
-[Kerrington Wells]
  Added original logic.


1.3.47
-----
-[Kerrington Wells]
  Added Datacenter logic.


1.3.46
-----
-[Kerrington Wells]
  Removed action create.

1.3.43
-----
-[Kerrington Wells]
  Changed Action nothing to action run in Set_releasedate recipie.

1.3.42
-----
-[Kerrington Wells]
  Added set release date recepie.


1.3.38
-----
-[Kerrington Wells]
  Added IP center links to code. Changed retry output.

1.3.37
-----
-[Kerrington Wells]
  Made currently password monitors variables strings instead of fixnum.

1.3.36
-----
-[Kerrington Wells]
   Added output for currently passing monitor counts.

1.3.35
-----
-[Kerrington Wells]
   Removed include statments from cookbook.


1.3.34
-----
-[Kerrington Wells]
   Removed ihg-bluestripe::Hosted from default.rb recepie.

1.3.33
-----
-[Kerrington Wells]
   Removed case from default.rb.

1.3.32
-----
-[Kerrington Wells]
   Added Startapp logic to default.rb to control execution flow of recepies.

1.3.31
-----
-[Kerrington Wells]
   Changed order of exectuion for bluestrip cookbooks.


1.3.30
-----
-[Kerrington Wells]
   Added case statment to common.rb.


1.3.29
-----
-[Kerrington Wells]
   Changed datacenter.name to datacenter name.


1.3.28
-----
-[Kerrington Wells]
   Added case statement to bluestripe hosted include.

1.3.27
-----
-[Kerrington Wells]
   Removed case from common_postpatch.rb

1.3.26
-----
-[Kerrington Wells]
   Created common_postpatch.rb to allow bluestripe cookbook exectution before service execution.

1.3.25
-----
-[Kerrington Wells]
   Added bluestripe logic to init script.

1.3.24
-----
-[Kerrington Wells]
   Took check_healthmonitors_uptime resource out of ruby block.

1.3.23
-----
-[Kerrington Wells]
   Added Node Health check and uptime downtime functionality.


1.3.22
-----
-[Kerrington Wells]
   Changed services attribue back to normal.

1.3.21
-----
-[Kerrington Wells]
   Changed Datacenter to datacenter.name.

1.3.20
-----
-[Kerrington Wells]
    Cleaned up Stopapp_prepatch file.

1.3.19
-----
-[Kerrington Wells]
    Changed datacenter name in case statement to Datacenter.

1.3.18
-----
-[Kerrington Wells]
	Added ruby block to execute node attributes at compile time.

1.3.17
-----
-[Kerrington Wells]
	Changed version from 1.3.6 to 1.3.17.

1.3.16
-----
-[Kerrington Wells]
	Added back list of services.

1.3.15
-----
-[Kerrington Wells]
	Making execution of Attributes class happend at compile time.

1.3.14
-----
-[Kerrington Wells]
	Wrapped service login in case statment.

1.3.13
-----
-[Kerrington Wells]
	Added Setattributes logic to common.rb.

1.3.12
-----
-[Kerrington Wells]
	Changed stopapp_cloundbolt to stopapp_prepatch and startapp_cloudbolt to startapp_postpatch.

1.3.11
-----
-[Kerrington Wells]
	Changed placement of bluestripe Hosted and default cookbooks.

1.3.10
-----
-[Kerrington Wells]
	Fixed mispelling of ihg-bluestripe::destroy reciepe.

1.3.9
-----
-[Kerrington Wells]
	Added cased for calling ihg-bluestripe default or Hosted based on datacenter.

1.3.8
-----
-[Kerrington Wells]
	Added startapp and stopapp cloundbolt.rb files.

1.3.7
-----
-[Kerrington Wells]
	Added Functionality that allows stopapp and startapp scripts to use tcserver b2c cookbooks for starting and stoping tcserver.


1.3.6
-----
-[Kerrington Wells]
	Added Functionality that allows stopapp and startapp scripts to use tcserver b2c cookbooks for starting and stoping tcserver.


1.3.5
-----
-[Birendra Singh]
	Cache location update - bug fix

1.3.2
-----
-[Birendra Singh]
	Services attribute defined test failing on "not defined"

1.2.1
-----
-[Kerrington Wells]
Delivery test

1.1.2
-----
- [Birendra Singh]
Sync up delivery/ git and cookbook


1.0.1
-----
- [Birendra Singh]
Add dependency upon node attribute "services"

services == a string of space separated list of services to be started or stopped in order.
[services.split.each]
Changes to common.rb only to populate node['apps']

0.1.0
-----
- [your_name] - Initial release of ihg-service

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
