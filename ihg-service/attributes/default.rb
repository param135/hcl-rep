#
# Cookbook:: ihg-service
# Attributes:: default
#
# Copyright:: 2018, IHG, All Rights Reserved.

default['log']['separator'] = "\n#{'*' * 80}\n"

if node['tags'].include? 'dg'
  default['services'] = 'gemfirecs'
elsif node['tags'].include? 'lc'
  default['services'] = 'gemfireloc'
end

env = ''
case node.chef_environment
when 'Integration'
  env = 'int'
when 'Development'
  default['ambari']['server'] = 'mksd1dlvisbi002.c.iron-potion-771.internal'
  default['ambari']['port'] = '8080'
when 'Staging'
  default['ambari']['server'] = 'mksd1dlvisbi002.c.iron-potion-771.internal'
  default['ambari']['port'] = '8080'
  env = 'staging'
when 'Performance'
  env = 'perf'
when 'Production'
  default['ambari']['server'] = 'mksd1dlvisbi002.c.iron-potion-771.internal'
  default['ambari']['port'] = '8080'
  env = 'prod'
end
default['jswapp']['environment'] = env
