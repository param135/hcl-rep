name             'ihg-service'
maintainer       'Birendra Singh'
maintainer_email 'singhb@ihg.com'
license          'All rights reserved'
description      'Installs/Configures ihg-service'
chef_version     '~> 12'
supports         'redhat'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.4.59'
issues_url       'http://sdlcscm.hiw.com/projects/CHOPS/repos/chef-ihg-service/' if respond_to?(:issues_url)
source_url       'http://sdlcscm.hiw.com/scm/chops/chef-ihg-service' if respond_to?(:source_url)
depends          'ihg-f5'
# depends          'ihg-eis-eda-kafka-cluster-solution'
