#
# Cookbook:: ihg-rapid7
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.
#
case node['platform']
when 'windows'
  include_recipe "#{cookbook_name}::Windows_Destroy"
else
  include_recipe "#{cookbook_name}::Linux_Destroy"
end
