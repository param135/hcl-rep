#
# Cookbook:: ihg-rapid7
# Recipe:: Windows_Install
#
# Copyright:: 2020, The Authors, All Rights Reserved.
#
if platform?('windows')
  directory 'C:\programdata\Rapid7-Agent' do
    action :create
  end

  windows_package 'Install Rapid7 Agent on Windows' do
    source node['ihg-rapid7']['source']
    options node['ihg-rapid7']['options']
    installer_type :msi
    not_if 'Get-Service -Name ir_agent'
    guard_interpreter :powershell_script
  end

  tag('rapid7_agent')
  ruby_block 'Rapid 7 Agent on success' do
    block do
      node.save unless node.override_runlist.empty?
    end
  end

  file 'C:\Program Files\Rapid7\Insight Agent\components\insight_agent\common\attributes.json' do
    content '{ "attributes": "Chef_Windows", "version": "1.0.0"}'
    only_if { File.exist?('C:\Program Files\Rapid7\Insight Agent\components\insight_agent\common\attributes.json') }
    notifies :restart, 'service[ir_agent]'
  end

  service 'ir_agent' do
    action [ :start ]
  end

end
