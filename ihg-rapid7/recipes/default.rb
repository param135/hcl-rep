#
# Cookbook:: ihg-rapid7
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.
#
if platform?('windows')
  include_recipe "#{cookbook_name}::Windows_Install"
else
  widfn = ''
  unless node['datacenter'].nil?
    widfn = node['datacenter']['workgroup'] unless node['datacenter']['workgroup'].nil?
    widfn += node['datacenter']['function'] unless node['datacenter']['function'].nil?
  end

  if widfn.eql?('rp7ap')
    include_recipe "#{cookbook_name}::scanengine_install"
  else
    include_recipe "#{cookbook_name}::Linux_Install"
  end
end
