#
# Cookbook: ihg-rapid7
# Recipe:: stop
#
# Copyright:: 2020, Jari.Pekkalainen@ihg.com, All Rights Reserved
#
case node['platform_family']
when 'rhel'

  service 'ir_agent' do
    action :stop
  end

end
