#
# Cookbook: ihg-rapid7
# Recipe:: linux_destroy
#
# Copyright:: 2020, Jari.Pekkalainen@ihg.com, All Rights Reserved
#
case node['platform_family']
when 'rhel'

  tar_extract "http://#{node['yumreposvr']}/repos/dist/rapid7/rapid7_agent_installer.tgz" do
    target_dir '/tmp/'
    only_if { File.exist?('/opt/rapid7/ir_agent/ir_agent') }
    notifies :run, 'bash[rapid7_destroy]', :immediately
  end

  service 'ir_agent' do
    action :stop
  end

  bash 'rapid7_destroy' do
    code <<-EOCODE
      chmod +x /tmp/rapid7_agent_installer/agent_installer.sh
      /tmp/rapid7_agent_installer/agent_installer.sh uninstall
    EOCODE
    action :nothing
    notifies :delete, 'directory[/tmp/rapid7_agent_installer]', :immediately
  end

  directory '/tmp/rapid7_agent_installer' do
    action :nothing
    recursive true
    ignore_failure true
  end

  untag('Rapid7-Agent') if tagged?('Rapid7-Agent')

  ruby_block 'Remove Rapid7 Agent tag on success' do
    block do
      node.save unless node.override_runlist.empty?
    end
  end

end
