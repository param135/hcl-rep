#
# Cookbook:: ihg-rapid7
# Recipe:: scanengine_deploy
#
# Copyright:: 2020, The Authors, All Rights Reserved.
#

# skip if console installed
return if ::File.exist?('/opt/rapid7/nexpose/nsc/nexposeconsole.rc')

# skip if ScaEngine already installed - Avoid tagging and node.save
return if ::File.exist?('/opt/rapid7/nexpose/nse/nexposeengine.rc')

# Download
remote_file '/tmp/rapid7_scanengine_installer.tgz' do
  source "http://#{node['yumreposvr']}/repos/dist/rapid7/rapid7_scanengine_installer.tgz"
  backup false
  not_if ::File.exist?('/opt/rapid7/nexpose/nse/nexposeengine.rc')
  notifies :run, 'bash[rapid7_scan_install]', :immediately
end

# Extract and install
bash 'rapid7_scan_install' do
  code <<-EOCODE
    cd /tmp
    tar -xf /tmp/rapid7_scanengine_installer.tgz
    chmod +x /tmp/rapid7_scanengine_installer/Rapid7Setup-Linux64.bin
	echo "y
      2
      2

      y
      #{node['hostname']}
      #{node['Datacenter']}
      IHG


      "| /tmp/rapid7_scanengine_installer/Rapid7Setup-Linux64.bin
  EOCODE
  action :nothing
  notifies :delete, 'directory[/tmp/rapid7_scanengine_installer]', :immediately
  notifies :delete, 'file[/tmp/rapid7_scanengine_installer.tgz]', :immediately
end

# Cleanup
file '/tmp/rapid7_scanengine_installer.tgz' do
  action :nothing
  ignore_failure true
end

directory '/tmp/rapid7_scanengine_installer' do
  action :nothing
  recursive true
  ignore_failure true
end

# Tag node
tag('Rapid7-ScanEngine')
ruby_block 'Rapid7 ScanEngine on success' do
  block do
    node.save unless node.override_runlist.empty?
  end
end
