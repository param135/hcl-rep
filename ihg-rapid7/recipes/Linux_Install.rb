#
# Cookbook: ihg-rapid7
# Recipe:: linux_install
#
# Copyright:: 2020, Jari.Pekkalainen@ihg.com, All Rights Reserved
#
case node['platform_family']
when 'rhel'

  remote_file '/tmp/rapid7_agent_installer.tgz' do
    source "http://#{node['yumreposvr']}/repos/dist/rapid7/rapid7_agent_installer.tgz"
    backup false
    not_if { File.exist?('/opt/rapid7/ir_agent/ir_agent') }
    notifies :run, 'bash[rapid7_install]', :immediately
  end

  bash 'rapid7_install' do
    code <<-EOCODE
      cd /tmp
      tar -xf /tmp/rapid7_agent_installer.tgz
      chmod +x /tmp/rapid7_agent_installer/agent_installer.sh
      /tmp/rapid7_agent_installer/agent_installer.sh install
    EOCODE
    action :nothing
    notifies :delete, 'directory[/tmp/rapid7_agent_installer]', :immediately
    notifies :delete, 'file[/tmp/rapid7_agent_installer.tgz]', :immediately
  end

  file '/tmp/rapid7_agent_installer.tgz' do
    action :nothing
    ignore_failure true
  end

  directory '/tmp/rapid7_agent_installer' do
    action :nothing
    recursive true
    ignore_failure true
  end

  service 'ir_agent' do
    action :start
  end

  tag('Rapid7-Agent')
  ruby_block 'Rapid7 Agent on success' do
    block do
      node.save unless node.override_runlist.empty?
    end
  end

  file '/opt/rapid7/ir_agent/components/insight_agent/common/attributes.json' do
    content '{ "attributes": "Chef_Linux", "version": "1.0.0"}'
    only_if { File.exist?('/opt/rapid7/ir_agent/components/insight_agent/common/attributes.json') }
    notifies :restart, 'service[ir_agent]'
  end

  service 'ir_agent' do
    action [ :start ]
  end

end
