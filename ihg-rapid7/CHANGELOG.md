ihg-rapid7 CHANGELOG
====================

0.6.4
------
- Giuliano Moschini, 11-09-2020. Updated .delivery.


0.6.3
------
- Giuliano Moschini, 11-09-2020. Added log parameter to attributes for Windows install.

0.6.0
------
- Praveen Chukkapalli, Apr 20, 
-- Fixed Windows Rapid7 Agent Installation code

0.5.8
------
- Birendra Singh, Apr 14, 
-- Apparently windows part is not completely validated. run is being disabled.

0.5.7
------
- Birendra Singh, Apr 14, 
-- Still AWS, version of ruby should be examined.

0.5.5
------
- Birendra Singh, Apr 14, 
-- Still AWS, changed defined to nil?

0.5.3
------
- Birendra Singh, Apr 14, 
-- cookstyle spaces

0.5.1
------
- Birendra Singh, Apr 14, 
-- Add ScanEngine recipe to install the same for rp7ap wid+fn

0.4.1
------
- Birendra Singh, Mar 30, 
-- Resubmit with delete downloaded distro file

0.4.0
------
- Birendra Singh, Mar 30, 
-- Remove dependency from cookbook tar

0.3.0
------
- Birendra Singh, Mar 29, Fix file name case in default for install recipes
- Cookstyle fixes only

0.1.1
------
- Praveen Chukkapalli - February 11, 2020 - Initial Release of Rapid7 Cookbook

0.1.0
------
- Praveen Chukkapalli - February 11, 2020 - Initial Release of Rapid7 Cookbook
