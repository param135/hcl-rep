# InSpec test for recipe ihg-rapid7::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

#  unless os.windows?
#    # This is an example test, replace with your own test.
#    describe user('root'), :skip do
#      it { should exist }
#    end
#  end
#
#  # This is an example test, replace it with your own test.
#  describe port(80), :skip do
#    it { should_not be_listening }
#  end
unless os.windows?
  # describe user('root'), :skip do
  describe user('root') do
    it { should exist }
  end

  describe file('/opt/rapid7/ir_agent/ir_agent') do
    it { should exist }
    it { should be_file }
    it { should be_executable }
    it { should be_owned_by 'root' }
  end

  describe service('ir_agent') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

end
