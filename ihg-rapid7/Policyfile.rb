# Policyfile.rb - Describe how you want Chef Infra Client to build your system.
#
# For more information on the Policyfile feature, visit
# https://docs.chef.io/policyfile.html

# A name that describes what the system you're building with Chef does.
name 'ihg-rapid7'

# Where to find external cookbooks:
default_source :supermarket
# default_source :chef_repo, "../"

# run_list: chef-client will run these recipes in the order specified.
run_list 'ihg-rapid7::default'

# Specify a custom source for a single cookbook:
cookbook 'ihg-rapid7', path: '.'
# cookbook 'tar', path: '../tar'
cookbook 'ihg-yumrepos', path: '../ihg-yumrepos'
