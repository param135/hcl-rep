name 'ihg-rapid7'
maintainer 'IHG Team'
maintainer_email 'Security Team'
license 'All Rights Reserved'
description 'Installs/Configures ihg-rapid7'

# depends 'tar'
depends 'ihg-yumrepos'

source_url 'http://sdlcscm.hiw.com/projects/CHOPS/repos/chef-ihg-rapid7/'
issues_url 'http://sdlcscm.hiw.com/projects/CHOPS/repos/chef-ihg-rapid7/'

chef_version '>= 12.20' if respond_to?(:chef_version)

supports 'centos'
supports 'fedora'
supports 'oracle'
supports 'redhat'
supports 'windows'

version '0.6.4'
