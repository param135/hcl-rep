#
# Cookbook:: symlink-ckb
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.

link '/usr/bin/gfsh' do
  to '/opt/pivotal/gemfire/bin/gfsh'
end
